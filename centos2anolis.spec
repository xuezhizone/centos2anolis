%define anolis_release 6

Name:           centos2anolis
Version:        0.1
Release:        %{anolis_release}
Summary:        Transfer CentOS to Anolis.

License:        MulanPSL2
URL:            www.openanolis.cn
Source0:        %{name}.py
Source1:        README.md
Source2:        LICENSE.txt

BuildRequires:  python3
Requires:       python3
Requires:       bash

BuildArch:      noarch

%description
This script is designed to automatically convert a CentOS instance to 
Anolis OS in-place. Currently it supports transferring from CentOS 7 
to Anolis OS 7 and from CentOS 8 to Anolis OS 8.

%prep
cp %{SOURCE1} .
cp %{SOURCE2} .


%install
mkdir -p %{buildroot}/%{_sbindir}
install -m 755 %{SOURCE0} %{buildroot}/%{_sbindir}


%files
%{_sbindir}/%{name}.py
%doc README.md
%license LICENSE.txt


%changelog
* Thu May 26 2022 XueZhixin <xuezhixin@uniontech.com> - 0.1-6
- Add offline migration

* Thu May 26 2022 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-5
- add custom log path
- add custom json file

* Fri Mar 25 2022 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-4
- fix module virt

* Fri Feb 11 2022 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-3
- Add mysql-server check
- handle 3rd-part repository files and try using an internal web address
- Add -c usage to continue migration
- save print message to /var/log/centos2anolis.log
- update README and LICENSE
- optimise code style
- add default version for switch

* Wed Jan 05 2022 Chunmei Xu <xuchunmei@linux.alibaba.com> - 0.1-2
- add check for i686 packages

* Wed Dec 22 2021 mgb0110571 <mgb01105731@alibaba-inc.com> - 0.1-1
- First CentOS2Anolis Package
