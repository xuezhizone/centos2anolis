#!/usr/bin/env python3
# Copyright (c) 2021-2022 OpenAnolis Community.
#
# Script to switch CentOS to the OpenAnolis yum repository.
#

import os
import subprocess
import re
import socket
import sys
import shutil
import argparse
import platform
import time
import urllib.request
import json
import logging

yum_url="https://mirrors.openanolis.org/anolis/"
github_url="https://gitee.com/anolis/centos2anolis"
repostr_an7 = '''[an7_os]
name=AnolisOS-7 - OS
baseurl=http://mirrors.openanolis.org/anolis/7/os/$basearch/os
gpgcheck=0
enabled=1

[an7_updates]
name=AnolisOS-7 - updates
baseurl=http://mirrors.openanolis.org/anolis/7/updates/$basearch/os
gpgcheck=0
enabled=1
'''
repostr_an8 = '''[an8_baseos]
name=AnolisOS-8 - BaseOS
baseurl=http://mirrors.openanolis.org/anolis/8/BaseOS/$basearch/os
gpgcheck=0
enabled=1

[an8_appstream]
name=AnolisOS-8 - AppStream
baseurl=http://mirrors.openanolis.org/anolis/8/AppStream/$basearch/os
gpgcheck=0
enabled=1
'''

old_packages = 'centos-backgrounds centos-logos centos-release centos-release-cr desktop-backgrounds-basic \
centos-release-advanced-virtualization centos-release-ansible26 centos-release-ansible-27 \
centos-release-ansible-28 centos-release-ansible-29 centos-release-azure \
centos-release-ceph-jewel centos-release-ceph-luminous centos-release-ceph-nautilus \
centos-release-ceph-octopus centos-release-configmanagement centos-release-dotnet centos-release-fdio \
centos-release-gluster40 centos-release-gluster41 centos-release-gluster5 \
centos-release-gluster6 centos-release-gluster7 centos-release-gluster8 \
centos-release-gluster-legacy centos-release-messaging centos-release-nfs-ganesha28 \
centos-release-nfs-ganesha30 centos-release-nfv-common \
centos-release-nfv-openvswitch centos-release-openshift-origin centos-release-openstack-queens \
centos-release-openstack-rocky centos-release-openstack-stein centos-release-openstack-train \
centos-release-openstack-ussuri centos-release-opstools centos-release-ovirt42 centos-release-ovirt43 \
centos-release-ovirt44 centos-release-paas-common centos-release-qemu-ev centos-release-qpid-proton \
centos-release-rabbitmq-38 centos-release-samba411 centos-release-samba412 \
centos-release-scl centos-release-scl-rh centos-release-storage-common \
centos-release-virt-common centos-release-xen centos-release-xen-410 \
centos-release-xen-412 centos-release-xen-46 centos-release-xen-48 centos-release-xen-common \
libreport-centos libreport-plugin-mantisbt libreport-plugin-rhtsupport python3-syspurpose \
python-oauth sl-logos yum-rhn-plugin centos-indexhtml'

reposdir =''
anolislist7 = ['7.7','7.9']
anolislist8 = ['8.2','8.4','8.5']
centosversion = 0
centosv = ""
internal_ecs = False
dst_release = ['anolis-release', 'anolis-repos', 'anolis-gpg-keys']
old_release_pkg = ""
jd={
    "Timestamp":0,
    "Progress":0,
    "ProgressInfo":"",
    "ErrorCode":0,
    "ErrorMsg":""
}
# Count of functions executed
jsoncount=0
# The total number of functions to be executed in ccontinue.
jsonsum=28
custom_json_file1=""

def get_current_version(continue_after_sync = False):
    global centosversion
    global centosv
    global jd

    log_it(logging.INFO, "Get current OS version.")
    crtversion = "centos"
    if continue_after_sync:
        log_it(logging.INFO, "Continue after sync.")
        crtversion = "anolis"
    try:
        centosv = str(subprocess.check_output("cat /etc/"+ crtversion +"-release | awk '{print $4}'", shell=True), 'utf-8')[:3]
        log_it(logging.INFO, "Version is " + centosv)
        float(centosv)
    except Exception as e:
        log_it(logging.ERROR, "Version not found, Please Check!!")
        jd["ErrorCode"] = 1
        jd["ErrorMsg"] = str(e)
        update_json_info()
        sys.exit(1)
    centosversion=int(float(centosv))
    if centosversion == 0:
        log_it(logging.ERROR, "Get current version failed.")
        sys.exit(1)

def check_pkg(pkg):
    if pkg.split('/')[0] == '':
        if os.path.exists(pkg):
            return True
        else:
            return False

    paths = os.environ['PATH'].split(':')
    for path in paths:
        if not os.path.isdir(path):
            continue
        for f in os.listdir(path):
            if os.path.isfile(os.path.join(path, f)):
                if f == pkg:
                    return True
    return False

def clean_and_exit():
    global repostr_an7
    global repostr_an8

    if centosversion == 7:
        repostr_an7 = re.sub(r"anolis/(.*)/", "anolis/7/", repostr_an7)
    if centosversion == 8:
        repostr_an8 = re.sub(r"anolis/(.*)/", "anolis/8/", repostr_an8)

    repo_path = os.path.join(reposdir, 'switch-to-anolis.repo')
    if os.path.exists(repo_path):
        os.remove(repo_path)
    sys.exit(1)

def get_disk_info(string):
    dev_name = ""
    part_name = ""
    length = len(string)
    for c in range(length-1, -1, -1):
        if not string[c].isdigit():
            if string.find('nvme') != -1:
                dev_name = string[0:c]
                part_num = string[c+1:length]
            else:
                dev_name = string[0:c+1]
                part_num = string[c+1:length]
            break
    return dev_name,part_num

def add_boot_option():
    log_it(logging.INFO, "Current system is uefi, add boot option to boot manager.")
    subprocess.run('which efibootmgr > /dev/null 2>&1 || yum install -y efibootmgr', shell=True)
    disk_name = subprocess.check_output('mount | grep /boot/efi | awk \'{print $1}\'', shell=True)
    disk_name = str(disk_name, 'utf-8')
    disk_name = disk_name.split('\n')[0]
    dev_name,part_num = get_disk_info(disk_name)
    if dev_name == "" or part_num == "":
        log_it(logging.INFO, "Parse /boot/efi disk info failed, update boot loader failed.")
        return

    cmd=""
    arch = platform.machine()
    if arch == "x86_64":
        cmd = 'efibootmgr -c -d ' + dev_name + ' -p ' + part_num + ' -l "/EFI/anolis/shimx64.efi" -L "Anolis OS"'
    elif arch == "aarch64":
        cmd = 'efibootmgr -c -d ' + dev_name + ' -p ' + part_num + ' -l "/EFI/anolis/shimaa64.efi" -L "Anolis OS"'
    try:
        subprocess.check_call(cmd, shell=True)
    except:
        log_it(logging.INFO, "Use efibootmgr update boot loader failed, please update boot loader manually.")

def change_repo_mirror(reposdir, prefix):
    mirror_openanolis1 = "mirrors.openanolis.org"
    mirror_openanolis2 = "mirrors.openanolis.cn"
    mirror_aliyun="mirrors.aliyun.com"

    log_it(logging.INFO, "Accelerating downloads...")
    anolis_repos = []
    files = os.listdir(reposdir)
    for f in files:
        repo_path=os.path.join(reposdir, f)
        if os.path.isfile(repo_path):
            if re.match(prefix, f):
                #replace
                with open(repo_path, 'r') as fr:
                    content = fr.read()
                    content = content.replace(mirror_openanolis1, mirror_aliyun).replace(mirror_openanolis2, mirror_aliyun)
                with open(repo_path, 'w') as fw:
                    fw.writelines(content)

def backup_repofiles():
    global old_release_pkg

    log_it(logging.INFO, "Backing up and removing old repository files...", True)
    repos = []
    if re.match('centos-release-' + str(centosversion) + '\.*|centos-linux-release-' + str(centosversion) + '\.*', old_release_pkg):
        old_version_r = subprocess.check_output('rpm -qa centos*repos', shell=True)
        old_version_r = str(old_version_r, 'utf-8')[:-1]
        old_release_pkg = old_release_pkg + " " + old_version_r
        if len(old_version_r) != 0:
            repos = subprocess.check_output("rpm -ql " + old_version_r + " | grep '\.repo$'", shell=True)
            repos = str(repos, 'utf-8').split('\n')[:-1]
        else:
            repos = subprocess.check_output("rpm -ql centos-release | grep '\.repo$'", shell=True)
            repos = str(repos, 'utf-8').split('\n')[:-1]

    num_centos_repos = subprocess.check_output('rpm -qa "centos-release-*" | wc -l', shell=True)
    if int(str(num_centos_repos,'utf-8')[0]) > 0:
        addtional_repos = subprocess.check_output('rpm -qla "centos-release-*"', shell=True)
        addtional_repos = str(addtional_repos, 'utf-8')
        if addtional_repos != '':
            addtional_repos = addtional_repos.split('\n')
            for r in addtional_repos:
                if re.match('.*\.repo$', r):
                    repos.append(r)

    backup_comment = '# This is a yum repository file that was disabled by\n' \
    + '# ' + __file__ + ', a script to convert CentOS to Anolis OS.\n' \
    + '# Please see ' + yum_url + ' for more information.\n\n'

    for repo in repos:
        if not os.path.isfile(repo):
            continue
        with open(repo, 'r') as fsrc:
            content = fsrc.read()
            with open(repo+'.disabled','w') as fdst:
                fdst.write(repo + '\n' + backup_comment+content)
        os.remove(repo)

def check_version(version):
    global repostr_an7
    global repostr_an8

    log_it(logging.INFO, "Checking the version of Anolis OS", True)
    if float(centosv) == 7.0:
        log_it(logging.INFO, "Set 'setenforce 0' in CentOS 7.0")
        subprocess.run('setenforce 0', shell=True)

    if not version:
        if centosversion == 7:
            repostr_an7 = re.sub(r"/(7)/", "/7.9/", repostr_an7)
            log_it(logging.INFO, "Switch to Anolis OS 7.9")
        elif centosversion == 8:
            log_it(logging.INFO, "Switch to Anolis OS 8")
        else:
            log_it(logging.ERROR, "CentOS version is not supported to switch.")
            sys.exit(1)
    else:
        if centosversion == 7:
            if (version in anolislist7):
                log_it(logging.INFO, "Switch to Anolis OS " + version)
                repostr_an7 = re.sub(r"/(7)/", "/" + version + "/", repostr_an7)
            else:
                log_it(logging.ERROR, "Version not supported, following versions are supported.")
                log_it(logging.ERROR, ' '.join(anolislist7))
                sys.exit(1)
        elif centosversion == 8:
            if version == "8":
                log_it(logging.INFO, "Switch to latest Anolis OS 8.")
            else:
                if (version not in anolislist8):
                    log_it(logging.ERROR, "Version not supported, following versions are supported.")
                    log_it(logging.INFO, ' '.join(anolislist8))
                    sys.exit(1)
                elif float(version) < float(centosv):
                    log_it(logging.WARNING, "Try to switch from CentOS " + centosv + " to Anolis OS " + version)
                    log_it(logging.WARNING, "OS migration does not support downgrade, please choose newer version!")
                    sys.exit(1)
                else:
                    log_it(logging.INFO, "Switch to Anolis OS " + version)
                    repostr_an8 = re.sub(r"/(8)/", "/" + version + "/", repostr_an8)
        else:
            log_it(logging.ERROR, "CentOS version is not supported to switch.")
            sys.exit(1)

def process_special_pkgs():
    log_it(logging.INFO, "Swap centos-logos related packages with Anolis OS packages", True)
    subprocess.run('rpm -q centos-logos-ipa && yum swap -y centos-logos-ipa anolis-logos-ipa', shell=True)
    subprocess.run('rpm -q centos-logos-httpd && yum swap -y centos-logos-httpd anolis-logos-httpd', shell=True)

    if centosversion == 7:
        subprocess.run('rpm -q redhat-lsb-core && yum -y remove redhat-lsb-core', shell=True)
        subprocess.run('rpm -q redhat-lsb-submod-security && yum -y remove redhat-lsb-submod-security',shell=True)
    else:
        log_it(logging.INFO, "Package redhat-lsb is replaced by system-lsb on Anolis OS")
        subprocess.run('rpm -q redhat-lsb-core && yum swap -y redhat-lsb-core system-lsb-core', shell=True)
        subprocess.run('rpm -q redhat-lsb-submod-security && yum swap -y redhat-lsb-submod-security system-lsb-submod-security',shell=True)

    log_it(logging.INFO, "Packages related to rhn are not provided by Anolis OS")
    subprocess.run('rpm -q rhn-client-tools && yum -y remove rhn-client-tools python3-rhn-client-tools python3-rhnlib', shell=True)

    log_it(logging.INFO, "Packages related to subscription are not provided by Anolis OS")
    subprocess.run('rpm -q subscription-manager && yum -y remove subscription-manager', shell=True)

    log_it(logging.INFO, "Package python3-syspurpose is not provided by Anolis OS")
    subprocess.run('rpm -q python3-syspurpose && yum -y remove python3-syspurpose', shell=True)

    log_it(logging.INFO, "Remove centos gpg-pubkey")
    subprocess.run('rpm -e $(rpm -q gpg-pubkey --qf "%{NAME}-%{VERSION}-%{RELEASE} %{PACKAGER}\\n" | grep CentOS | awk \'{print $1}\')', shell=True)

def process_pkgs_for_c7():
    log_it(logging.INFO, "process pkgs for c7", True)
    if centosversion != 7:
        return

    # 7.0-7.1
    subprocess.run("rpm -q gnome-documents && yum -y remove gnome-documents", shell=True)
    # 7.0-7.3
    # downgrade  mythes-en & hyphen-en
    subprocess.run("rpm -q mythes-en hyphen-en && yum -y downgrade mythes-en hyphen-en", shell=True)
    subprocess.run("rpm -q cdparanoia-libs && yum -y downgrade cdparanoia-libs", shell=True)
    subprocess.run("rpm -q libvirt-client && yum -y upgrade libvirt-client", shell=True)
    subprocess.run("rpm -q gnome-packagekit && yum -y upgrade gnome-packagekit*", shell=True)
    subprocess.run("rpm -q libreoffice-langpack-en && yum -y upgrade libreoffice-langpack-en", shell=True)
    subprocess.run("rpm -q unoconv && yum -y update unoconv", shell=True)
    #7.0-7.1 remove centos-bookmarks
    subprocess.run("rpm -q centos-bookmarks && yum -y remove centos-bookmarks", shell=True)
    # remove kmod-kvdo
    subprocess.run("rpm -q kmod-kvdo && yum -y remove kmod-kvdo", shell=True)
    # 7.9 remove libreport plugin
    subprocess.run("rpm -q libreport-plugin-rhtsupport && yum -y remove libreport-plugin-rhtsupport", shell=True)
    subprocess.run("rpm -q libreport-centos && yum -y remove libreport-centos", shell=True)
    subprocess.run("rpm -q libreport-plugin-mantisbt && yum -y remove libreport-plugin-mantisbt", shell=True)
    subprocess.run("rpm -q libreport-rhel-anaconda-bugzilla && yum -y remove libreport-rhel-anaconda-bugzilla", shell=True)
    subprocess.run('yum -y downgrade systemd systemd-libs systemd-python systemd-sysv libgudev1', shell=True)

def get_version_from_rpm(string):
    length = len(string)
    release_pos = -1
    version = ""
    for c in range(length-1, -1, -1):
        if string[c] == '-':
            if release_pos == -1:
                release_pos = c
            else:
                version = string[c+1:release_pos]
        if version != "":
            break

    if version.find(':') != -1:
        version = version.split(':')[1]

    return version

def check_mysql(version):
    log_it(logging.INFO, 'Checking mysql version', True)
    mysql_install = False
    try:
        subprocess.check_call('rpm -q mysql-server', shell=True)
        mysql_install = True
    except:
        pass

    if not mysql_install or centosversion != 8:
        return

    install_pkg = subprocess.check_output('rpm -q mysql-server', shell=True).decode().split('\n')[0]
    install_ver = get_version_from_rpm(install_pkg)
    log_it(logging.INFO, "Get mysql version from anolis repos")
    arch = platform.machine()
    ver = "8"
    if version:
        ver = version
    url = "http://mirrors.aliyun.com/anolis/" + ver + "/AppStream/" + arch + "/os/"
    if internal_ecs:
        url = url.replace("mirrors.aliyun.com", "mirrors.cloud.aliyuncs.com")
    cmd = 'repoquery --repofrompath=an-test,' + url + ' --disablerepo=* --enablerepo=an-test mysql-server --disable-modular-filtering -q | sort -r'
    repo_pkg = subprocess.check_output(cmd, shell=True).decode().split('\n')[0]
    if len(repo_pkg) == 0:
        return
    repo_version = get_version_from_rpm(repo_pkg)
    log_it(logging.INFO, "Install mysql version is " + install_ver + ", and anolis mysql version is " + repo_version)
    if install_ver > repo_version:
        log_it(logging.INFO, "Install mysql version is higher than anolis mysql version, cannot migrate to anolis.")
        sys.exit(1)

def process_yum_utils_install(internal_ecs):
    log_it(logging.INFO, "Process yum-utils install")
    yum_utils_install = False
    try:
        subprocess.check_call("yum -y install yum-utils", shell=True)
        yum_utils_install = True
    except:
        log_it(logging.INFO, "Install yum-utils failed, try to use vault repos.")

    if yum_utils_install:
        return

    if centosversion != 8:
        return

    subprocess.run("yum -y install yum-utils --disablerepo=* -c /tmp/c8_vault.repo --enablerepo=c8_vault*", shell=True)
    os.remove('/tmp/c8_vault.repo')
    if not check_pkg('yumdownloader'):
        clean_and_exit()

def log_it(level=logging.INFO, msg="", js=False):
    if js:
        update_json_info(msg)
    print(msg)
    logging.log(level, msg)

def set_log_info(custom_log_dir):
    fn = '/var/log/centos2anolis.log'
    if custom_log_dir:
        if custom_log_dir[0] == '/' and '\\' not in custom_log_dir:
            if not os.path.exists(custom_log_dir):
                os.makedirs(custom_log_dir)
            fn = custom_log_dir + '/centos2anolis.log'
        else:
            print("Please enter a directory in the correct format")
            sys.exit(1)
    logging.basicConfig(
        filename=fn,
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d[%(levelname)-8s]: %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"
    )

def check_user():
    log_it(logging.INFO, "Checking if the tool is executed by root user")
    # check if the script is executed by root user
    if os.geteuid() != 0:
        log_it(logging.ERROR, "Please run the tool as root user.")
        sys.exit(1)

def check_required_pkgs():
    # check required packages
    log_it(logging.INFO, 'Checking required packages', True)
    for pkg in ['rpm', 'yum', 'curl']:
        if not check_pkg(pkg):
            log_it(logging.ERROR, "Could not found " + pkg)
            sys.exit(1)

def check_i686_pkgs():
    log_it(logging.INFO, 'Checking i686 packages', True)
    i686_pkgs = subprocess.check_output('rpm -qa --qf "%{NAME}-%{VERSION}-%{RELEASE}.%{ARCH} %{VENDOR}\n" | grep CentOS | grep i686 | awk \'{print $1}\'', shell=True)
    i686_pkgs = str(i686_pkgs, 'utf-8')
    if len(i686_pkgs) != 0:
        log_it(logging.WARNING, "Anolis OS does not provide i686 packages, please remove them before migration.")
        log_it(logging.WARNING, "Installed i686 packages:")
        log_it(logging.WARNING, i686_pkgs)
        sys.exit(1)

def verify_before(verify_all_rpms):
    if verify_all_rpms:
        log_it(logging.INFO, "Creating a list of RPMs installed before the switch", True)
        log_it(logging.INFO, "Verifying RPMs installed before the switch against RPM database")
        out1 = subprocess.check_output('rpm -qa --qf \
               "%{NAME}|%{VERSION}|%{RELEASE}|%{INSTALLTIME}|%{VENDOR}|%{BUILDTIME}|%{BUILDHOST}|%{SOURCERPM}|%{LICENSE}|%{PACKAGER}\\n" \
               | sort > "/var/tmp/$(hostname)-rpms-list-before.log"', shell=True)
        out2 = subprocess.check_output('rpm -Va | sort -k3 > "/var/tmp/$(hostname)-rpms-verified-before.log"',shell=True)
        files = os.listdir('/var/tmp/')
        hostname = socket.gethostname()
        log_it(logging.INFO, "Review the output of following files:")
        for f in files:
            if re.match(hostname+'-rpms-(.*)\.log', f):
                log_it(logging.INFO, f)
    else:
        log_it(logging.INFO, "Verify before", True)

def check_distribution():
    global old_release_pkg

    # check if the os old_version is supported
    log_it(logging.INFO, "Checking: distribution", True)
    old_version = subprocess.check_output("rpm -q --whatprovides /etc/redhat-release", shell=True)
    old_version = str(old_version, 'utf-8')
    old_version = old_version.split('\n')[:-1]
    if len(old_version) == 0:
        log_it(logging.ERROR, "You appear to be running an unsupported distribution.")
        sys.exit(1)
    if len(old_version) > 1:
        log_it(logging.ERROR,"Could not determine your distribution because multiple packages are providing redhat-release:")
        log_it(logging.ERROR, '\n'.join(old_version))
        sys.exit(1)

    old_release_pkg = old_version[0]
    log_it(logging.INFO, old_release_pkg)
    if re.match('anolis-release', old_release_pkg):
        log_it(logging.ERROR, "You are already using Anolis OS.")
        sys.exit(1)

    elif re.match('centos-linux-release', old_release_pkg):
        subver = old_release_pkg.split('-')[3]

    elif re.match('redhat-release|centos-release|sl-release', old_release_pkg):
        subver = old_release_pkg.split('-')[2]

    else:
        log_it(logging.ERROR, "Your are using an unsupported distribution.")
        sys.exit(1)

    if not re.match(str(centosversion),subver):
        log_it(logging.ERROR, "You appear to be running an unsupported distribution.")
        sys.exit(1)

def check_yum_lock():
    log_it(logging.INFO, "Checking: yum lock", True)
    if os.path.exists('/var/run/yum.pid'):
        with open('/var/run/yum.pid', 'r') as f:
            pid = f.read()
            with open('/proc/' + pid + '/comm', 'r') as ff:
                comm = ff.read()
                log_it(logging.ERROR, 'Another app is currently holding the yum lock: ' + comm)
                log_it(logging.ERROR, 'Running as pid: ' + pid)
                log_it(logging.ERROR, 'Please kill it and run the tool again.')
        sys.exit(1)

def check_dnf_modules():
    global internal_ecs
    global jd
    if centosversion != 8:
        log_it(logging.INFO, "Check dnf modules", True)
        return

    # check dnf
    log_it(logging.INFO, "Checking: dnf modules enable list", True)
    log_it(logging.INFO, "Identifying dnf modules that are enabled...")

    repostr_centos_vault_8 = '''[c8_vault_baseos]
name=c8_vault - BaseOS
baseurl=http://mirrors.aliyun.com/centos-vault/8.5.2111/BaseOS/$basearch/os/
gpgcheck=0
enabled=1

[c8_vault_appstream]
name=c8_vault - AppStream
baseurl=http://mirrors.aliyun.com/centos-vault/8.5.2111/AppStream/$basearch/os/
gpgcheck=0
enabled=1
'''

    if internal_ecs:
        repostr_centos_vault_8 = repostr_centos_vault_8.replace("mirrors.aliyun.com", "mirrors.cloud.aliyuncs.com")
    with open('/tmp/c8_vault.repo', 'w') as f:
        f.write(repostr_centos_vault_8)
    
    enabled_modules = str(
        subprocess.check_output("dnf module list -c /tmp/c8_vault.repo --enabled | grep rhel | awk '{print $1}'", shell=True),
        'utf-8')
    enabled_modules = enabled_modules.split('\n')[:-1]
    try:
        with open('/root/enabled_modules.json', 'w') as f:
            json.dump(enabled_modules, f)
    except Exception as e:
        log_it(logging.ERROR, "Save enabled_modules file failed")
        jd["ErrorCode"] = 1
        jd["ErrorMsg"] = str(e)
        update_json_info()
        sys.exit(1)
    unknown_mods = []
    if len(enabled_modules) > 0:
        for mod in enabled_modules:
            if re.fullmatch('container-tools|llvm-toolset|virt', mod):
                subprocess.run('dnf module reset -c /tmp/c8_vault.repo -y ' + mod, shell=True)
            if not re.fullmatch('container-tools|go-toolset|jmc|llvm-toolset|rust-toolset|virt', mod):
                unknown_mods.append(mod)
        if len(unknown_mods) > 0:
            log_it(logging.WARNING, 'This tool is unable to automatically switch module(s) ' \
                  + ','.join(unknown_mods) \
                  + ' from a CentOS \'rhel\' stream to an Anolis OS equivalent.' \
                  )
            opt = input('Do you want to continue and resolve it manually? (Yes or No)\n' + \
                        'You may want select No to stop and raise an issue on ' \
                        + github_url \
                        + ' for advice. ' \
                        )
            if opt != 'Yes':
                    sys.exit(1)

def check_network():
    global internal_ecs

    log_it(logging.INFO, "Checking network environment", True)
    dst_url = "http://mirrors.cloud.aliyuncs.com"
    internal_ecs = False
    try:
        dst_status = urllib.request.urlopen(dst_url, timeout=5).code
        internal_ecs = True
        log_it(logging.INFO, "Check internal network environment is True")
    except Exception as err:
        log_it(logging.ERROR, err)
        pass

def check_yumdownloader():
    log_it(logging.INFO, "Looking for yumdownloader", True)
    if not check_pkg('yumdownloader'):
        process_yum_utils_install(internal_ecs)

def check_repodir():
    global reposdir

    log_it(logging.INFO, "Finding your repository directory", True)
    dir = '/etc/yum.repos.d/'
    if os.path.isdir(dir):
        reposdir = dir
    else:
        log_it(logging.ERROR, "Cannot find repository directory")
        sys.exit(1)

    if len(reposdir) == 0:
        log_it(logging.ERROR, "Could not locate your repository directory.")
        sys.exit(1)

def process_third_repos():
    log_it(logging.INFO, "Processing contentdir in repo files...", True)
    subprocess.run("sed -i \'s/\$contentdir/centos/g\' /etc/yum.repos.d/*.repo", shell=True)

    log_it(logging.INFO, "Backing up and removing 3rd-part repository files...")
    subprocess.run("mkdir /etc/yum.repos.d/migration-bak", shell=True)
    subprocess.run("mv $(find /etc/yum.repos.d/*.repo) /etc/yum.repos.d/migration-bak", shell=True)

def write_switch_repo(accelerate):
    log_it(logging.INFO, "switch to anolis.repo", True)
    repofile = os.path.join(reposdir, 'switch-to-anolis.repo')
    with open(repofile, 'w') as f:
        if centosversion == 7:
            repostr_an=repostr_an7
        else:
            repostr_an=repostr_an8
        f.write(repostr_an)
    if accelerate:
        change_repo_mirror(reposdir, 'switch-to-anolis.repo')
    if internal_ecs:
        subprocess.run('sed -i \'s/mirrors.openanolis.org/mirrors.cloud.aliyuncs.com/g\' /etc/yum.repos.d/switch-to-anolis.repo', shell=True)
        subprocess.run('sed -i \'s/mirrors.openanolis.cn/mirrors.cloud.aliyuncs.com/g\' /etc/yum.repos.d/switch-to-anolis.repo', shell=True)
        subprocess.run('sed -i \'s/mirrors.aliyun.com/mirrors.cloud.aliyuncs.com/g\' /etc/yum.repos.d/switch-to-anolis.repo', shell=True)

def modify_yum_conf():
    log_it(logging.INFO, "Removing CentOS-specific yum configuration from /etc/yum.conf ...", True)
    with open('/etc/yum.conf', 'r') as f:
        content = f.read()
    if re.search(r'^distroverpkg=', content, re.MULTILINE):
        content = re.sub(r"\n(distroverpkg=)", r"\n#\1", content)
    if re.search(r'bugtracker_url=', content, re.MULTILINE):
        content = re.sub(r"\n(bugtracker_url=)", r"\n#\1", content)
    with open('/etc/yum.conf', 'w') as f:
        f.write(content)

def download_anolis_release():
    global jd
    log_it(logging.INFO, "Downloading Anolis OS release package...", True)
    try:
        stat = subprocess.check_output("yumdownloader "+' '.join(dst_release), shell=True)
    except Exception as e:
        log_it(logging.ERROR, "Could not download the following packages from " + yum_url)
        log_it(logging.ERROR, '\n'.join(dst_release))
        log_it(logging.ERROR, "")
        log_it(logging.ERROR, "Are you behind a proxy? If so, make sure the 'http_proxy' environmen")
        log_it(logging.ERROR, "variable is set with your proxy address.")
        log_it(logging.ERROR, "An error occurred while attempting to switch this system to Anolis OS" + \
        "and it may be in an unstable/unbootable state. To avoid further issues, " +\
        "the script has terminated.")
        jd["ErrorCode"] = 1
        jd["ErrorMsg"] = str(e)
        update_json_info()
        clean_and_exit()

def switch_release_pkgs(local_flag):
    log_it(logging.INFO, "Switching old release package with Anolis OS...", True)
    dst_rpms = [s + '*.rpm' for s in dst_release]
    subprocess.run('rpm -i --force ' + ' '.join(dst_rpms) + ' --nodeps', shell=True)
    if centosversion == 7:
        subprocess.run('rpm -e --nodeps ' + old_release_pkg, shell=True)
    else:
        subprocess.run('rpm -e --nodeps ' + old_release_pkg + ' centos-gpg-keys', shell=True)
    if not local_flag:
        os.remove(os.path.join(reposdir, 'switch-to-anolis.repo'))
    else:
        local_disabled_release_repo()

def optimise_repofile(accelerate):
    log_it(logging.INFO, "optimise repofile", True)
    if accelerate:
        change_repo_mirror(reposdir, 'AnolisOS-')

    if internal_ecs:
        subprocess.run('sed -i \'s/mirrors.openanolis.cn/mirrors.cloud.aliyuncs.com/g\' /etc/yum.repos.d/*.repo', shell=True)
        subprocess.run('sed -i \'s/mirrors.openanolis.org/mirrors.cloud.aliyuncs.com/g\' /etc/yum.repos.d/*.repo', shell=True)
        subprocess.run('sed -i \'s/mirrors.aliyun.com/mirrors.cloud.aliyuncs.com/g\' /etc/yum.repos.d/*.repo', shell=True)

def install_base_pkgs():
    base_packages=['basesystem','initscripts','anolis-logos','plymouth','grub2','grubby']

    log_it(logging.INFO, "Installing base packages for Anolis OS...", True)
    subprocess.run('yum clean all', shell=True)
    cmd = 'yum shell -y <<EOF\n\
remove ' + old_packages + '\n\
install ' + ' '.join(base_packages) + '\n\
run\n\
EOF'
    subprocess.run(cmd, shell=True)

    if os.access('/usr/libexec/plymouth/plymouth-update-initrd', os.X_OK):
        log_it(logging.INFO, "Updating initrd...")
        subprocess.run('/usr/libexec/plymouth/plymouth-update-initrd')

def process_enabled_modules():
    global jd
    if centosversion != 8:
        log_it(logging.INFO, "Process enabled modules", True)
        return

    log_it(logging.INFO, "Process enabled modules after migration.", True)
    try:
        with open('/root/enabled_modules.json', 'r') as f:
            enabled_modules = json.load(f)
    except Exception as e:
        log_it(logging.ERROR, "Read enabled_modules file failed")
        sys.exit(1)

    os.remove('/root/enabled_modules.json')

    if len(enabled_modules) > 0:
        for mod in enabled_modules:
            subprocess.run('dnf module reset -y '+mod, shell=True)
            if re.fullmatch('container-tools|go-toolset|jmc|llvm-toolset|rust-toolset', mod):
                subprocess.run('dnf module enable -y '+mod+':an8', shell=True)
            elif mod =='virt':
                subprocess.run('dnf module enable -y '+mod+':an', shell=True)
            else:
                log_it(logging.WARNING, "Unsure how to transform module"+mod)
        subprocess.run('dnf -y distro-sync', shell=True)

    try:
        subprocess.check_call('dnf module list --enabled | grep satellite-5-client', shell=True)
        log_it(logging.INFO, "Anolis OS does not provide satellite-5-client module, disable it.")
        subprocess.run('dnf module disable -y satellite-5-client', shell=True)
    except:
        pass

def remove_yum_cache():
    log_it(logging.INFO, "Removing yum cache", True)
    if os.path.isfile('/var/cache/yum'):
        os.remove('/var/cache/yum')
    elif os.path.isdir('/var/cache/yum'):
        shutil.rmtree('/var/cache/yum')
    if centosversion ==8:
        if os.path.isfile('/var/cache/dnf'):
            os.remove('/var/cache/dnf')
        elif os.path.isdir('/var/cache/dnf'):
            shutil.rmtree('/var/cache/dnf')

def verify_after(verify_all_rpms=False):
    if verify_all_rpms:
        log_it(logging.INFO, "Creating a list of RPMs installed after the switch", True)
        log_it(logging.INFO, "Verifying RPMs installed after the switch against RPM database")
        out1 = subprocess.check_output('rpm -qa --qf \
        "%{NAME}|%{VERSION}|%{RELEASE}|%{INSTALLTIME}|%{VENDOR}|%{BUILDTIME}|%{BUILDHOST}|%{SOURCERPM}|%{LICENSE}|%{PACKAGER}\\n" \
        | sort > "/var/tmp/$(hostname)-rpms-list-after.log"', shell=True)
        out2 = subprocess.check_output('rpm -Va | sort -k3 > "/var/tmp/$(hostname)-rpms-verified-after.log"',shell=True)
        files = os.listdir('/var/tmp/')
        hostname = socket.gethostname()
        log_it(logging.INFO, "Review the output of following files:")
        for f in files:
            if re.match(hostname+'-rpms-(.*)\.log', f):
                log_it(logging.INFO, f)
    else:
        log_it(logging.INFO, "Verify after", True)

def update_grub_cfg():
    log_it(logging.INFO, "Sync successfully, update grub.cfg.", True)
    if os.path.isdir('/sys/firmware/efi'):
        subprocess.run('grub2-mkconfig -o /boot/efi/EFI/anolis/grub.cfg', shell=True)
        add_boot_option()
    else:
        subprocess.run('grub2-mkconfig -o /boot/grub2/grub.cfg', shell=True)
        try:
            subprocess.check_call('test -L /boot/grub2/grubenv', shell=True)
            log_it(logging.INFO, "On leagcy system, /boot/grub2/grubenv is softlink, process it.")
            subprocess.run('mv /boot/grub2/grubenv /boot/grub2/grubenv-bak', shell=True)
            subprocess.run('cat /boot/grub2/grubenv-bak > /boot/grub2/grubenv', shell=True)
        except:
            pass

def resume_third_repos():
    log_it(logging.INFO, "Resume 3rd-part repository files...", True)
    subprocess.run("mv /etc/yum.repos.d/migration-bak/* /etc/yum.repos.d", shell=True)
    subprocess.run("rm -rf /etc/yum.repos.d/migration-bak", shell=True)

def reset_yum_cache():
    log_it(logging.INFO, "Yum clean all", True)
    subprocess.run("yum clean all", shell=True)
    subprocess.run("yum makecache", shell=True)

def do_migration():
    log_it(logging.INFO, "Distro sync", True)
    global jd
    try:
        subprocess.check_call('yum update libdnf -y', shell=True)
        subprocess.check_call('yum -y distro-sync', shell=True)
    except Exception as e:
        log_it(logging.ERROR, "Could not automatically sync with Anolis OS repositories.\n\
        Check the output of 'yum distro-sync' to manually resolve the issue.")
        log_it(logging.ERROR, "After you resolve the issue, Please use 'python3 centos2anolis.py -c ' to continue migration")
        jd["ErrorCode"] = 1
        jd["ErrorMsg"] = str(e)
        update_json_info()
        sys.exit(1)

def set_json_info(custom_json_file):
    jn='/var/log/centostoanolis.json'
    if custom_json_file:
        if custom_json_file.startswith('/') and custom_json_file.endswith('.json') and '\\' not in custom_json_file:
            jp="/".join(custom_json_file.split("/")[:-1])
            if not os.path.exists(jp):
                os.makedirs(jp)
            if not os.path.exists(custom_json_file):
                with open(custom_json_file,"w") as f:
                    json.dump(jd,f)
        else:
            print("Please enter a directory in the correct format")
            sys.exit(1)
    else:
        with open(jn,"w") as f:
            json.dump(jd,f)

def update_json_info(info=False):
    global jd
    global jsoncount
    global custom_json_file1
    jd["Timestamp"] = int(time.time())
    if info:
        jsoncount += 1
        jd["ProgressInfo"] = info
    jd["Progress"]= int(jsoncount / jsonsum * 100)
    if custom_json_file1:
        with open(custom_json_file1, "w") as f:
            json.dump(jd,f)
    else:
        with open("/var/log/centostoanolis.json", "w") as f:
            json.dump(jd,f)


def local_disabled_release_repo():
    path = '/etc/yum.repos.d'
    if os.path.exists(path):
        file_list = os.listdir(path)
    for file in file_list:
        fpath = os.path.join(path, file)
        if os.path.isdir(fpath):
            continue
        else:
            if re.fullmatch('switch-to-anolis.repo', file, re.IGNORECASE):
                continue
            elif not re.search('repo$', file, re.IGNORECASE):
                continue
            with open(fpath, 'r') as fdst:
                allrepo = fdst.read()
                fdst.close()
                with open(fpath + '.disabled', 'w+') as fdsta:
                    fdsta.write(
                        '#This is a yum repository file that was disabled . \n' + allrepo)
                    fdsta.close()
                    os.remove(fpath)


def check_local_repo(localrepo):
    if not localrepo:
        return
    repopath = '/etc/yum.repos.d/switch-to-anolis.repo'
    if os.path.exists(repopath):
        local_disabled_release_repo()
        return
    else:
        log_it(logging.ERROR, "Please write the file of the anolis repository: /etc/yum.repos.d/switch-to-anolis.repo")
        sys.exit(1)

def main(verify_all_rpms=False, accelerate=False, version=False, continue_after_sync=False, custom_log_dir = False, custom_json_file=False, local_repo=False):
    global jd
    global jsoncount
    global custom_json_file1
    set_log_info(custom_log_dir)
    set_json_info(custom_json_file)
    custom_json_file1=custom_json_file
    log_it(logging.INFO, "====== Start ======")
    check_user()
    check_local_repo(local_repo)
    get_current_version(continue_after_sync)
    if not continue_after_sync:
        try:
            check_version(version)
            check_required_pkgs()
            check_i686_pkgs()
            verify_before(verify_all_rpms)
            check_distribution()
            check_yum_lock()
            check_network()
            check_dnf_modules()
            check_yumdownloader()
            check_mysql(version)
            check_repodir()
            if not local_repo:
                backup_repofiles()
                process_third_repos()
                write_switch_repo(accelerate)
            modify_yum_conf()
            download_anolis_release()
            switch_release_pkgs(local_repo)
            optimise_repofile(accelerate)
            install_base_pkgs()
            process_pkgs_for_c7()
        except Exception as e:
            jd["ErrorCode"] = 1
            jd["ErrorMsg"] = str(e)
            update_json_info()
            log_it(logging.INFO, "Switch successful. Syncing with Anolis OS repositories.")

    try:
        jsoncount = 20
        do_migration()
        process_enabled_modules()
        process_special_pkgs()
        remove_yum_cache()
        verify_after(verify_all_rpms)
        update_grub_cfg()
        resume_third_repos()
        reset_yum_cache()
    except Exception as e:
        jd["ErrorCode"] = 1
        jd["ErrorMsg"] = str(e)
        update_json_info()
    log_it(logging.INFO, "Switch complete. Anolis OS recommends rebooting this system.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-V', action='store_true', help='Verify RPM information before and after the switch')
    parser.add_argument('-s', action='store_true', help='Accelerate download')
    parser.add_argument('-v', help='Choose the version of Anolis OS')
    parser.add_argument('-c', action='store_true', help='Continue to migration after distro-sync')
    parser.add_argument('--log_dir', help='Set Custom log directory')
    parser.add_argument('--progress_file', help='Set Custom json file')
    parser.add_argument('-l', action='store_true', help='Select the local repo file')
    args = parser.parse_args()
    sys.exit(main(args.V, args.s, args.v, args.c, args.log_dir, args.progress_file, args.l))


